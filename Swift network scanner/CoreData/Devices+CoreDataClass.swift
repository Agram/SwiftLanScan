//
//  Devices+CoreDataClass.swift
//  Swift network scanner
//
//  Created by Sergey Evdokimov on 11.02.2018.
//  Copyright © 2018 Sergey Evdokimov. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Devices)
public class Devices: NSManagedObject {
    
    convenience init() {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "Devices"), insertInto: CoreDataManager.instance.managedObjectContext)
    }
    
    convenience init(device: MMDevice, network: Networks) {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "Devices"), insertInto: CoreDataManager.instance.managedObjectContext)
        self.hostname = device.hostname
        self.brand = device.brand
        self.ip = device.ipAddress
        self.mac = device.macAddress
        self.network = network
        self.ports = ["0"]
    }
}
