//
//  Devices+CoreDataProperties.swift
//  Swift network scanner
//
//  Created by Sergey Evdokimov on 11.04.2018.
//  Copyright © 2018 Sergey Evdokimov. All rights reserved.
//
//

import Foundation
import CoreData


extension Devices {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Devices> {
        return NSFetchRequest<Devices>(entityName: "Devices")
    }

    @NSManaged public var brand: String?
    @NSManaged public var hostname: String?
    @NSManaged public var ip: String?
    @NSManaged public var mac: String?
    @NSManaged public var ports: [String]?
    @NSManaged public var network: Networks?

}
