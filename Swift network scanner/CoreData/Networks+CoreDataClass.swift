//
//  Networks+CoreDataClass.swift
//  Swift network scanner
//
//  Created by Sergey Evdokimov on 11.02.2018.
//  Copyright © 2018 Sergey Evdokimov. All rights reserved.
//
//

import Foundation
import CoreData

@objc(Networks)
public class Networks: NSManagedObject {
    
    convenience init() {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "Networks"), insertInto: CoreDataManager.instance.managedObjectContext)
    }
    
    convenience init(networkHost device: MMDevice) {
        self.init(entity: CoreDataManager.instance.entityForName(entityName: "Networks"), insertInto: CoreDataManager.instance.managedObjectContext)
        
        self.ip = device.ipAddress
        self.hostname = device.hostname
        self.brand = device.brand
        self.favorite = false
        self.date = Date()
    }
    
}

extension Date
{
    func toString( dateFormat format  : String ) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
}
