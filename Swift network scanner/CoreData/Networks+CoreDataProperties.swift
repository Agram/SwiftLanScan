//
//  Networks+CoreDataProperties.swift
//  Swift network scanner
//
//  Created by Sergey Evdokimov on 14.04.2018.
//  Copyright © 2018 Sergey Evdokimov. All rights reserved.
//
//

import Foundation
import CoreData


extension Networks {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Networks> {
        return NSFetchRequest<Networks>(entityName: "Networks")
    }

    @NSManaged public var brand: String?
    @NSManaged public var date: Date?
    @NSManaged public var favorite: Bool
    @NSManaged public var hostname: String?
    @NSManaged public var ip: String?
    @NSManaged public var ssid: String?
    @NSManaged public var mac: String?
    @NSManaged public var devices: NSSet?

}

// MARK: Generated accessors for devices
extension Networks {

    @objc(addDevicesObject:)
    @NSManaged public func addToDevices(_ value: Devices)

    @objc(removeDevicesObject:)
    @NSManaged public func removeFromDevices(_ value: Devices)

    @objc(addDevices:)
    @NSManaged public func addToDevices(_ values: NSSet)

    @objc(removeDevices:)
    @NSManaged public func removeFromDevices(_ values: NSSet)

}
