//
//  DetailTableViewController.swift
//  Swift network scanner
//
//  Created by Sergey Evdokimov on 04.02.2018.
//  Copyright © 2018 Sergey Evdokimov. All rights reserved.
//

import UIKit
import SwiftSocket

class DetailTableViewController: UITableViewController, PortScannerDelegate {
    
    var detailItem: Devices!
    var scanner: PortScanner!
    var portsDictionary: NSDictionary?
    let settings = UserDefaults.standard
    var portsIsScanning: Bool?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.scanner = PortScanner(delegate: self)
        portsIsScanning = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(startPortScanning))
        self.navigationItem.rightBarButtonItem?.isEnabled = !portsIsScanning!

        // Update the user interface for the detail item.
        if let detailItem = detailItem {
            self.navigationItem.title = detailItem.ip
        }
        
        //Load content of ports.plist into resourceFileDictionary dictionary
        if let path = Bundle.main.path(forResource: "ports", ofType: "plist") {
            portsDictionary = NSDictionary(contentsOfFile: path)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.rightBarButtonItem?.isEnabled = !portsIsScanning!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return section == 0 ? "Device informmation" : "Ports"
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return section == 0 ? settings.bool(forKey: "Mac") ? settings.bool(forKey: "Brand") ? 4 : 3 : 2 : detailItem?.ports?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Configure the cell..
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
                cell?.textLabel?.text = "Hostname:"
                cell?.detailTextLabel? .text = detailItem?.hostname
                return cell!
            }
            if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
                cell?.textLabel?.text = "Ip address:"
                cell?.detailTextLabel?.text = detailItem?.ip
                return cell!
            }
            if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
                cell?.textLabel?.text = "Mac address:"
                cell?.detailTextLabel?.text = detailItem?.mac
                return cell!
            }
            if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
                cell?.textLabel?.text = "Brand:"
                cell?.detailTextLabel?.text = detailItem?.brand == "" ? "iOS Error. Brand not found." : detailItem?.brand
                return cell!
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
            if !detailItem.ports!.contains("0") {
                cell.textLabel?.text = detailItem.ports![indexPath.item]
                cell.detailTextLabel?.text = portsDictionary!.value(forKey: detailItem.ports![indexPath.item]) as? String
            } else {
                cell.textLabel?.text = ""
                cell.detailTextLabel?.text = portsDictionary!.value(forKey: detailItem.ports![indexPath.item]) as? String
            }
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0: break
        case 1:
          portDetailActionSheet(indexPath)
        default: break
        }
    }
    
    // MARK: - Alerts
    func portDetailActionSheet(_ indexPath: IndexPath) {
        let alert = UIAlertController(title: detailItem.ports![indexPath.item], message: portsDictionary!.value(forKey: detailItem.ports![indexPath.item]) as? String, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: { _ in
            NSLog("Closed")
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = tableView.cellForRow(at: indexPath)!.frame
        }
        self.present(alert, animated: true, completion: nil)
    }

    // MARC: - Port Scanner
    
    @objc func startPortScanning() {
        portsIsScanning = true
        scanner.host = detailItem.ip! as String
        let backgroundQueue = DispatchQueue(label: "portScanner", qos: .background)
        backgroundQueue.async {
            self.scanner.sendButtonAction()
        }
    }
    
    func PortScannerStart() {
        print("Starting")
        DispatchQueue.main.sync {
            self.navigationItem.rightBarButtonItem?.isEnabled = false
        }
    }
    
    func PortScannerPortFound(port: Int) {
        
        if !detailItem.ports!.contains(String(port)) {
            detailItem.ports!.append(String(port))
        }
        if detailItem.ports!.contains("0") {
            detailItem.ports!.remove(at: (detailItem.ports?.index(of: "0"))!)
        }
        
        CoreDataManager.instance.saveContext()
        DispatchQueue.main.sync {
            self.tableView.reloadSections([1], with: .automatic)
        }
    }
    
    func PortScannerStop() {
        DispatchQueue.main.sync {
            portsIsScanning = false
            self.navigationItem.rightBarButtonItem?.isEnabled = true
        }
        print("Stop")
    }
}
