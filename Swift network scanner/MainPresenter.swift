//
//  MainPresenter.swift
//  Swift network scanner
//
//  Created by Sergey Evdokimov on 18.02.2018.
//  Copyright © 2018 Sergey Evdokimov. All rights reserved.
//

import UIKit
import Foundation
import SystemConfiguration.CaptiveNetwork

protocol MainPresenterDelegate {
    func mainPresenterIPSearchFinished()
    func mainPresenterIPSearchCancelled()
    func mainPresenterIPSearchFailed()
}

class MainPresenter: NSObject, MMLANScannerDelegate {
    
    @objc dynamic var connectedDevices : [Devices]!
    var network: Networks?
    @objc dynamic var progressValue : Float = 0.0
    @objc dynamic var isScanRunning : BooleanLiteralType = false
    
    var lanScanner : MMLANScanner!
    var delegate : MainPresenterDelegate?
    
    //MARK: - Custom init method
    //Initialization with delegate
    init(delegate:MainPresenterDelegate?){
        super.init()
        self.delegate = delegate!
        self.connectedDevices = [Devices]()
        self.isScanRunning = false
        self.lanScanner = MMLANScanner(delegate:self)
    }
    
    //MARK: - Button Actions
    //This method is responsible for handling the tap button action on MainTableViewController. In case the scan is running and the button is tapped it will stop the scan
    func scanButtonClicked() -> Void {
        if (self.isScanRunning) {
            self.stopNetWorkScan()
        }
        else {
            self.startNetWorkScan()
        }
    }
    
    func startNetWorkScan() -> Void{
        if (self.isScanRunning) {
            self.stopNetWorkScan()
            self.connectedDevices.removeAll()
        }
        else {
            self.connectedDevices.removeAll()
            self.isScanRunning = true
            self.lanScanner.start()
        }
    }
  
    func stopNetWorkScan() -> Void{
        self.lanScanner.stop()
        self.isScanRunning = false
    }
    
    // MARK: - WiFi SSID and Mac Info
    
    func getWiFiInfo(key: String) -> String? {
        var wifiInfo: String?
        if let interfaces = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interfaces){
                let interfaceName = CFArrayGetValueAtIndex(interfaces, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                guard let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString) else {
                    return wifiInfo
                }
                guard let interfaceData = unsafeInterfaceData as? [String: Any] else {
                    return wifiInfo
                }
                switch key {
                case "ssid":
                    guard let SSID = interfaceData["SSID"] as? String else {
                        return wifiInfo
                    }
                    wifiInfo = SSID
                case "mac":
                    guard let BSSID = interfaceData["BSSID"] as? String else {
                        return wifiInfo
                    }
                    wifiInfo = BSSID
                default:
                    wifiInfo = nil
                }
            }
        }
        return wifiInfo == nil ? "No WiFi Connected" : wifiInfo
    }
    
    // MARK: - MMLANScanner Delegates
    //The delegate methods of MMLANScanner
    func lanScanDidFindNewDevice(_ mmdevice: MMDevice!) {
        let ipSortDescriptor = NSSortDescriptor(key: "ip", ascending: true)
        
        //Adding the found device in the array
        
        if mmdevice.hostname != nil && mmdevice.macAddress != nil {
            
            if self.connectedDevices.count == 0 {
                network = Networks(networkHost: mmdevice)
                network!.hostname = self.getWiFiInfo(key: "ssid")
                network!.ssid = self.getWiFiInfo(key: "ssid")
                network!.mac = self.getWiFiInfo(key: "mac")
            }
            
            let device = Devices(device: mmdevice, network: network!)
            if !self.connectedDevices.contains(device) {
                self.connectedDevices?.append(device)
            }
        }
        self.connectedDevices = (self.connectedDevices as NSArray).sortedArray(using: [ipSortDescriptor]) as! Array
    }
    
    func lanScanDidFailedToScan() {
        self.isScanRunning = false
        self.delegate?.mainPresenterIPSearchFailed()
    }
    
    func lanScanDidFinishScanning(with status: MMLanScannerStatus) {
        self.isScanRunning = false
        //Checks the status of finished. Then call the appropriate method
        if (status == MMLanScannerStatusFinished) {
            self.delegate?.mainPresenterIPSearchFinished()
        }
        else if (status == MMLanScannerStatusCancelled) {
            self.delegate?.mainPresenterIPSearchCancelled()
        }
    }
    
    func lanScanProgressPinged(_ pingedHosts: Float, from overallHosts: Int) {
        //Updating the progress value. MainTableViewController will be notified by KVO
        self.progressValue = pingedHosts / Float(overallHosts)
    }

}
