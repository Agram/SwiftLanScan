//
//  MainTableViewCell.swift
//  Swift network scanner
//
//  Created by Sergey Evdokimov on 02.02.2018.
//  Copyright © 2018 Sergey Evdokimov. All rights reserved.
//

import UIKit
import CoreData

class MainTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
 
    func refresh(_ device: Devices) {
        self.textLabel?.text = device.ip
        self.detailTextLabel?.text = device.hostname
    }
    
}
