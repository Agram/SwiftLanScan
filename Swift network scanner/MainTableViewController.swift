//
//  MainTableViewController.swift
//  Swift network scanner
//
//  Created by Sergey Evdokimov on 01.02.2018.
//  Copyright © 2018 Sergey Evdokimov. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController, MainPresenterDelegate {
        
    private var myContext = 0
    var presenter: MainPresenter!
    var refresh = UIRefreshControl()
    
    @IBOutlet weak var progressView: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.refresh.addTarget(self, action: #selector(refreshSearch), for: .valueChanged)
        self.refresh.tintColor = UIColor.black
        
        tableView.addSubview(refresh)
        
        self.progressView.isHidden = true
        self.progressView.progress = 0

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshSearch))
        
        //Init presenter. Presenter is responsible for providing the business logic of the MainTableViewController (MVVM)
        self.presenter = MainPresenter(delegate:self)
        
        //Add observers to monitor specific values on presenter. On change of those values MainTableViewController UI will be updated
        self.addObserversForKVO()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = self.presenter.getWiFiInfo(key: "ssid")
    }
    
    @objc func refreshSearch() {
        self.progressView.progress = 0
        self.progressView.isHidden = false
        self.presenter.scanButtonClicked()
    }
 
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - KVO Observers
    func addObserversForKVO () -> Void {
        self.presenter.addObserver(self, forKeyPath: "connectedDevices", options: .new, context:&myContext)
        self.presenter.addObserver(self, forKeyPath: "progressValue", options: .new, context:&myContext)
        self.presenter.addObserver(self, forKeyPath: "isScanRunning", options: .new, context:&myContext)
    }
    
    func removeObserversForKVO () -> Void {
        self.presenter.removeObserver(self, forKeyPath: "connectedDevices")
        self.presenter.removeObserver(self, forKeyPath: "progressValue")
        self.presenter.removeObserver(self, forKeyPath: "isScanRunning")
    }
    
    //MARK: - Presenter Delegates
    //The delegates methods from Presenters.These methods help the MainPresenter to notify the MainTableViewController for any kind of changes
    func mainPresenterIPSearchFinished() {
        self.progressView.isHidden = true
        refresh.endRefreshing()
        self.showAlert(title: "Scan Finished", message: "Number of devices connected to the Local Area Network : \(self.presenter.connectedDevices.count)")
    }
    
    func mainPresenterIPSearchCancelled() {
        self.progressView.isHidden = true
        refresh.endRefreshing()
        self.tableView.reloadData()
    }
    
    func mainPresenterIPSearchFailed() {
        self.progressView.isHidden = true
        refresh.endRefreshing()
        self.showAlert(title: "Failed to scan", message: "Please make sure that you are connected to a WiFi before starting LAN Scan")
    }
    
    //MARK: - Alert Controller
    func showAlert(title:String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in}
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: - KVO
    //This is the KVO function that handles changes on MainPresenter
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if (context == &myContext) {
            switch keyPath! {
            case "connectedDevices":
                self.tableView.reloadSections([0], with: .automatic)
            case "progressValue":
                self.progressView.progress = self.presenter.progressValue
            case "isScanRunning":
                let isScanRunning = change?[.newKey] as! BooleanLiteralType
                self.navigationItem.rightBarButtonItem = isScanRunning ? UIBarButtonItem(barButtonSystemItem: .pause, target: self, action: #selector(refreshSearch)) : UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(refreshSearch))
            default:
                print("Not valid key for observing")
            }
        }
    }
    
    //MARK: - Deinit
    deinit {
        self.removeObserversForKVO()
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.presenter.connectedDevices!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? MainTableViewCell {
            let item = self.presenter.connectedDevices[indexPath.row]
            cell.refresh(item)
            return cell
        }
        return UITableViewCell()
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            self.presenter.connectedDevices.remove(at: indexPath.row)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = self.presenter.connectedDevices[indexPath.row]
        performSegue(withIdentifier: "showDetail", sender: object)
    }

    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showDetail" {
            let controller = segue.destination as! DetailTableViewController
            controller.detailItem = sender as! Devices
            controller.navigationItem.leftItemsSupplementBackButton = true
        }
    }
    
}

