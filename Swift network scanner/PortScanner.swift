//
//  PortScanner.swift
//  Swift network scanner
//
//  Created by Sergey Evdokimov on 02.04.2018.
//  Copyright © 2018 Sergey Evdokimov. All rights reserved.
//

import Foundation
import SwiftSocket

protocol PortScannerDelegate {
    func PortScannerStart()
    func PortScannerPortFound(port: Int)
    func PortScannerStop()
}

class PortScanner: NSObject {
    
    var delegate: PortScannerDelegate
    
    var host = "apple.com"
    var ports: [Int]?
    var tcpClient: TCPClient?
    
    init(delegate: PortScannerDelegate?){
        self.delegate = delegate!
        if let portsFromSettings = UserDefaults.standard.array(forKey: "Ports") {
            ports = portsFromSettings as? [Int]
        } else{
            ports = [21, 22, 23, 25, 53, 80, 110, 115, 135, 139, 143, 194, 443, 445, 1080, 1433, 3306, 3389, 5900]
            UserDefaults.standard.set(ports, forKey: "Ports")
        }
    }

    func sendButtonAction() {
        self.delegate.PortScannerStart()
        for port in ports! {
            tcpClient = TCPClient(address: host, port: Int32(port))
            guard let client = tcpClient else { return }
            switch client.connect(timeout: 1) {
            case .success:
                print("Connected to \(client.address):\(port)")
                if let response = sendRequest(string: "GET / HTTP/1.0\n\n", using: client) {
                    print("Response: \(response)")
                }
                self.delegate.PortScannerPortFound(port: port)
            case .failure(let error):
                print(String(describing: error))
            }
            client.close()
        }
        self.delegate.PortScannerStop()
    }
    
    private func sendRequest(string: String, using client: TCPClient) -> String? {        
        switch client.send(string: string) {
        case .success:
            return readResponse(from: client)
        case .failure(let error):
            print(String(describing: error))
            return nil
        }
    }
    
    private func readResponse(from client: TCPClient) -> String? {
        guard let response = client.read(1024*10^2) else { return nil }
        return String(bytes: response, encoding: .utf8)
    }
    
}
