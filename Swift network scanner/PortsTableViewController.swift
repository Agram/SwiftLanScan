//
//  PortsTableViewController.swift
//  Swift network scanner
//
//  Created by Sergey Evdokimov on 23.04.2018.
//  Copyright © 2018 Sergey Evdokimov. All rights reserved.
//

import UIKit

class PortsTableViewController: UITableViewController {
    
    let settings = UserDefaults.standard
    var ports: [Int]?
    var portsDictionary: NSDictionary?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Scanning ports"

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        let addBatton: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addPortAlert(_:)))
        self.navigationItem.leftItemsSupplementBackButton = true
        self.navigationItem.setRightBarButtonItems([self.editButtonItem, addBatton], animated: true)

        
        if let path = Bundle.main.path(forResource: "ports", ofType: "plist") {
            portsDictionary = NSDictionary(contentsOfFile: path)
        }
        ports = settings.object(forKey: "Ports") as? [Int]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return ports!.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = "\(ports![indexPath.row])"
        cell.detailTextLabel?.text = portsDictionary!.value(forKey: String(ports![indexPath.row])) as? String
        cell.selectionStyle = .none
        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            ports!.remove(at: indexPath.row)
            settings.set(ports, forKey: "Ports")
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        portDetailActionSheet(indexPath)
    }
    
    // MARK: - Alerts
    func portDetailActionSheet(_ indexPath: IndexPath) {
        let alert = UIAlertController(title: "\(ports![indexPath.row])", message: portsDictionary!.value(forKey: String(ports![indexPath.row])) as? String, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: { _ in
            NSLog("Closed")
        }))
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = tableView.cellForRow(at: indexPath)!.frame
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    @objc func addPortAlert(_ sender: AnyObject) {
        let alert = UIAlertController(title: "Enter port", message: nil, preferredStyle: .alert)
        alert.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter port"
        }
        alert.addAction(UIAlertAction(title: "Add", style: .default, handler: { _ in
            let firstTextField = alert.textFields![0] as UITextField
            if (self.portsDictionary!.value(forKey: firstTextField.text!) != nil) && !(self.ports?.contains(Int(firstTextField.text!)!))!{
                self.ports!.append(Int(firstTextField.text!)!)
                self.ports!.sort()
                self.settings.set(self.ports, forKey: "Ports")
                self.tableView.reloadSections([0], with: .automatic)
                let alert = UIAlertController(title: "Added", message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: { _ in
                    NSLog("Add")
                }))
                self.present(alert, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: "Port cant be detect", message: nil, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: { _ in
                    NSLog("No description for port")
                }))
                self.present(alert, animated: true, completion: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: "Close", style: .cancel, handler: { _ in
            NSLog("Closed")
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
