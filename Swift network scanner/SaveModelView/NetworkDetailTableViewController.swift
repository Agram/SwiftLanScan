//
//  NetworkDetailTableViewController.swift
//  Swift network scanner
//
//  Created by Sergey Evdokimov on 15.02.2018.
//  Copyright © 2018 Sergey Evdokimov. All rights reserved.
//

import UIKit
import CoreData

class NetworkDetailTableViewController: UITableViewController {
    
    var network: Networks?
    var devices: [Devices]?
    
    var fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Devices")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let result = NSPredicate(format: "network = %@", (network?.objectID)!)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "ip", ascending: true)]
        fetchRequest.predicate = result
        
        do {
            devices = try CoreDataManager.instance.managedObjectContext.fetch(fetchRequest) as? [Devices]
        } catch {
            print(error)
        }

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareSheet))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func networkToJSON(_ data: Networks) -> String {
        let json = """
{
    "ssid": "\(String(describing: network!.ssid!))",
    "hostname": "\(String(describing: network!.hostname!))",
    "ip": "\(String(describing: network!.ip!))",
    "mac": "\(String(describing: network!.mac!))",
    "brand": "\(String(describing: network!.brand!))",
    "date": "\(String(describing: network!.date!))",
    "devices": [
"""
        func addJSONdevises() -> String{
            var devicesString: String = ""
            
            for device in devices! {
                devicesString = devicesString + "\n     {\n"+"""
                        "hostname": "\(String(describing: device.hostname!))",
                        "ip": "\(String(describing: device.ip!))",
                        "mac": "\(String(describing: device.mac!))",
                        "brand": "\(String(describing: device.brand!))",
                        "ports": "\(String(describing: device.ports!.description))"
                        },
                """
            }
            devicesString = devicesString + """
            \n    ]
            }
            """
            return devicesString
        }
        
        return json + addJSONdevises()
    }
    
    func networkToString(_ coreDataObject: AnyObject) -> String {
        return String(describing: coreDataObject)
    }
    
    @objc func shareSheet(_ sender: UIBarButtonItem) {
        
        let messageData = UserDefaults.standard.bool(forKey: "Share") ? networkToString(network as AnyObject) : networkToJSON(network!)

        
        do {
            let networkInfo = "Network SSID: \(String(describing: network!.ssid!)) with \(String(describing: network!.devices!.count)) devices"
            let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
            let documentsDirectory = urls[urls.count-1]
            let fileURL = documentsDirectory.appendingPathComponent("\(String(describing: network!.ssid!)).json")
            try messageData.write(to: fileURL, atomically: true, encoding: .utf8)
            let activityViewController = UIActivityViewController(activityItems: [networkInfo, fileURL], applicationActivities: nil)
            activityViewController.popoverPresentationController?.barButtonItem = sender
            self.present(activityViewController, animated: true, completion: nil)
        } catch {
            let nserror = error as NSError
            let alert = UIAlertController(title: "Error", message: "\(nserror.userInfo)", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            alert.popoverPresentationController?.barButtonItem = sender
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func switchChanged(fromSwitch uiswitch: UISwitch) {
        network?.favorite = uiswitch.isOn
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 2
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Network information"
        } else {
            return "Devices:"
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return 5
        } else {
            return network?.devices?.count ?? 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Configure the cell..
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "EditCell")
                cell?.textLabel?.text = "Name"
                let textField = UITextField(frame: CGRect(x: 0, y: 0, width: cell!.frame.width*0.75, height: 44))
                textField.textAlignment = .right
                textField.placeholder = network?.hostname
                textField.addTarget(self, action: #selector(cellEditingDidEnd), for: .editingDidEndOnExit)
                textField.returnKeyType = .done
                cell?.accessoryView = textField
                return cell!
            }
            if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
                cell?.textLabel?.text = "SSID"
                cell?.detailTextLabel?.text = network?.ssid
                return cell!
            }
            if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
                cell?.textLabel?.text = "MAC"
                cell?.detailTextLabel?.text = network?.mac
                return cell!
            }
            if indexPath.row == 3 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
                cell?.textLabel?.text = "Create date"
                cell?.detailTextLabel?.text = network?.date!.toString(dateFormat: "HH:mm dd/MM")
                return cell!
            }
            if indexPath.row == 4 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Favorite")
                cell?.textLabel?.text = "Favorite"
                let favSwitch = UISwitch(frame: .null)
                favSwitch.addTarget(self, action: #selector(switchChanged), for: UIControlEvents.valueChanged)
                favSwitch.isOn = network!.favorite
                cell!.accessoryView = favSwitch
                return cell!
            }
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DeviceCell", for: indexPath) as! MainTableViewCell
                let device = devices![indexPath.row]
                cell.refresh(device)
                return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section != 0 {
            let object = devices![indexPath.row]
            performSegue(withIdentifier: "showDetail", sender: object)
        }
        
    }
    
    @objc func cellEditingDidEnd(_ sender: UITextField) {
        network?.hostname = sender.text
        CoreDataManager.instance.saveContext()
    }
    

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showDetail" {
            let controller = segue.destination as! DetailTableViewController
            controller.detailItem = sender as? Devices
            controller.navigationItem.leftItemsSupplementBackButton = true
        }
    }
}
