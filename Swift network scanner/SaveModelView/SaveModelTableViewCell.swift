//
//  SaveModelTableViewCell.swift
//  Swift network scanner
//
//  Created by Sergey Evdokimov on 06.02.2018.
//  Copyright © 2018 Sergey Evdokimov. All rights reserved.
//

import UIKit
import FontAwesome_swift

class SaveModelTableViewCell: UITableViewCell {

    @IBOutlet weak var networkLabel: UILabel!
    @IBOutlet weak var deviceLabel: UILabel!
    @IBOutlet weak var favoriteLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func refresh(_ model: Networks) {
        if model.hostname != nil {
            networkLabel.text = model.hostname
        }else {
            networkLabel.text = model.ssid
        }
        deviceLabel.text = String(describing: model.devices!.count)
        
        switch model.favorite {
        case true:
            favoriteLabel.font = UIFont.fontAwesome(ofSize: 14)
            favoriteLabel.text = String.fontAwesomeIcon(name: .star)
        case false:
            favoriteLabel.text = ""
        }
    }
}
