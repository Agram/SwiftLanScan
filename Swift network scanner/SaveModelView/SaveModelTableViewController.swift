//
//  SaveModelTableViewController.swift
//  Swift network scanner
//
//  Created by Sergey Evdokimov on 01.02.2018.
//  Copyright © 2018 Sergey Evdokimov. All rights reserved.
//

import UIKit
import CoreData

class SaveModelTableViewController: UITableViewController {
        
    var fetchedResultsController = CoreDataManager.instance.fetchedResultsController(entityName: "Networks", keyForSort: "date")
    let settings = UserDefaults.standard
    var tableViewSorting: Bool?
    let segmentedControl = UISegmentedControl(items: ["All networks","Favorite"])
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewSorting = settings.bool(forKey: "Favorite")
        
        //MARK: - CoreDate
        favoriteSortPredicate()

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        self.navigationItem.leftBarButtonItem = self.editButtonItem
        segmentedControl.addTarget(self, action: #selector(sorting), for: UIControlEvents.valueChanged)
        segmentedControl.selectedSegmentIndex = NSNumber(value: settings.bool(forKey: "Favorite")) as! Int
        self.navigationItem.titleView = segmentedControl
    }
    
    override func viewWillAppear(_ animated: Bool) {
        CoreDataManager.instance.saveContext()
        favoriteSortPredicate()
        tableView.reloadSections([0], with: .automatic)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Sorting
    
    @objc func sorting() {
        if tableViewSorting! {
            tableViewSorting = false
            favoriteSortPredicate()

        } else {
            tableViewSorting = true
            favoriteSortPredicate()
        }
    }
    
    func favoriteSortPredicate() {
        if !tableViewSorting! {
            fetchedResultsController = CoreDataManager.instance.fetchedResultsController(entityName: "Networks", keyForSort: "date")
            do {
                try fetchedResultsController.performFetch()
            } catch {
                print(error)
            }
            tableView.reloadSections([0], with: .automatic)
        } else {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Networks")
            fetchRequest.sortDescriptors = [NSSortDescriptor(key: "date", ascending: false)]
            fetchRequest.predicate = NSPredicate(format: "favorite = true")
            fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataManager.instance.managedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
            do {
                try fetchedResultsController.performFetch()
            } catch {
                print(error)
            }
            tableView.reloadSections([0], with: .automatic)
        }
    }
    
    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if let sections = fetchedResultsController.sections {
            return sections[section].numberOfObjects
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? SaveModelTableViewCell {
            let item = fetchedResultsController.object(at: indexPath) as! Networks
            cell.refresh(item)
            return cell
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            self.tableView.beginUpdates()
            let managerObject = fetchedResultsController.object(at: indexPath) as! NSManagedObject
            CoreDataManager.instance.managedObjectContext.delete(managerObject)
            CoreDataManager.instance.saveContext()
            do {
                try fetchedResultsController.performFetch()
            } catch {
                print(error)
            }
            self.tableView.deleteRows(at: [indexPath], with: .automatic)
            self.tableView.endUpdates()
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let object = fetchedResultsController.object(at: indexPath) as? Networks
        performSegue(withIdentifier: "networkDetail", sender: object)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "networkDetail" {
            let controller = segue.destination as! NetworkDetailTableViewController
            controller.network = sender as? Networks
            controller.navigationItem.leftItemsSupplementBackButton = true
        }
    }
}
