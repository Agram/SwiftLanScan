//
//  SettingsTableViewController.swift
//  Swift network scanner
//
//  Created by Sergey Evdokimov on 01.02.2018.
//  Copyright © 2018 Sergey Evdokimov. All rights reserved.
//

import UIKit

class SettingsTableViewController: UITableViewController {
    
    let settings = UserDefaults.standard
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Settings"
        
        // Watch user default changes
        UserDefaults.standard.addObserver(self, forKeyPath: "Mac", options: NSKeyValueObservingOptions.new, context: nil)
        
        // Ports init
        portsInit()

    }
    
    func portsInit() {
        // Ports init
        if UserDefaults.standard.array(forKey: "Ports") == nil {
            UserDefaults.standard.set([21, 22, 23, 25, 53, 80, 110, 115, 135, 139, 143, 194, 443, 445, 1080, 1433, 3306, 3389, 5900], forKey: "Ports")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Observers for UserDefaults
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        settings.set(false, forKey: "Brand")
        tableView.reloadSections([1], with: .automatic)
    }

    deinit {
        UserDefaults.standard.removeObserver(self, forKeyPath: "Mac")
    }
    
    // MARK: - UISwitch change events
    @objc func switchChangedFavorite(fromSwitch uiSwitch: UISwitch) {
        settings.set(uiSwitch.isOn, forKey: "Favorite")
    }
    
    @objc func switchChangedShare(fromSwitch uiSwitch: UISwitch) {
        settings.set(uiSwitch.isOn, forKey: "Share")
    }
    
    @objc func switchChangedMac(fromSwitch uiSwitch: UISwitch) {
        settings.set(uiSwitch.isOn, forKey: "Mac")
    }
    
    @objc func switchChangedBrand(fromSwitch uiSwitch: UISwitch) {
        settings.set(uiSwitch.isOn, forKey: "Brand")
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 3
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Global"
        case 1:
            return "Device information"
        case 2:
            return "Default"
        default:
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
        switch section {
        case 0:
            return "Application settings"
        case 1:
            return "Search algoritms settings"
        case 2:
            return "Application data settings"
        default:
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return section == 0 ? 2 : 3
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "Favorite")
                cell.textLabel?.text = "Favorite first"
                cell.detailTextLabel?.text = "Show favorite network first"
                cell.selectionStyle = .none
                let uiSwitch = UISwitch(frame: .null)
                uiSwitch.addTarget(self, action: #selector(switchChangedFavorite), for: UIControlEvents.valueChanged)
                uiSwitch.isOn = settings.bool(forKey: cell.reuseIdentifier!)
                cell.accessoryView = uiSwitch
                return cell
            }
            if indexPath.row == 1 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "Share")
                cell.textLabel?.text = "Share CoreData instead of JSON"
                cell.detailTextLabel?.text = "Export CoreData object"
                cell.selectionStyle = .none
                let uiSwitch = UISwitch(frame: .null)
                uiSwitch.addTarget(self, action: #selector(switchChangedShare), for: UIControlEvents.valueChanged)
                uiSwitch.isOn = settings.bool(forKey: cell.reuseIdentifier!)
                cell.accessoryView = uiSwitch
                return cell
            }
        case 1:
            if indexPath.row == 0 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "Mac")
                cell.textLabel?.text = "Show Mac address"
                cell.detailTextLabel?.text = "Try to get device mac address"
                cell.selectionStyle = .none
                let uiSwitch = UISwitch(frame: .null)
                uiSwitch.addTarget(self, action: #selector(switchChangedMac), for: UIControlEvents.valueChanged)
                uiSwitch.isOn = settings.bool(forKey: cell.reuseIdentifier!)
                cell.accessoryView = uiSwitch
                return cell
            }
            if indexPath.row == 1 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "Brand")
                cell.textLabel?.text = "Show device brand"
                cell.detailTextLabel?.text = "Try to identify device brand name (Need Mac address)"
                cell.selectionStyle = .none
                let uiSwitch = UISwitch(frame: .null)
                uiSwitch.addTarget(self, action: #selector(switchChangedBrand), for: UIControlEvents.valueChanged)
                uiSwitch.isOn = settings.bool(forKey: cell.reuseIdentifier!)
                uiSwitch.isEnabled = settings.bool(forKey: "Mac")
                cell.accessoryView = uiSwitch
                return cell
            }
            if indexPath.row == 2 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
                cell.textLabel?.text = "Scanning ports"
                let ports = settings.object(forKey: "Ports") as! [Int]
                cell.detailTextLabel?.text = String(ports.count)
                cell.selectionStyle = .none
                return cell
            }
        case 2:
            if indexPath.row == 0 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "RestoreSettings")
                cell.textLabel?.text = "Restore default settings"
                cell.selectionStyle = .none
                return cell
            }
            if indexPath.row == 1 {
                let cell = UITableViewCell(style: .default, reuseIdentifier: "RemoveNonFavorite")
                cell.textLabel?.text = "Remove non-favorites network"
                cell.textLabel?.textColor = UIColor.red
                cell.selectionStyle = .none
                return cell
            }
            if indexPath.row == 2 {
                let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "RemoveAllCoreData")
                cell.textLabel?.text = "Remove all application data"
                cell.textLabel?.textColor = UIColor.red
                cell.detailTextLabel?.text = "Remove all data and restore default settings"
                cell.selectionStyle = .none
                return cell
            }
        default: break
        }
        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case 0: break
        case 1: break
        case 2:
            if indexPath.row == 0 {
                let appDomain = Bundle.main.bundleIdentifier!
                UserDefaults.standard.removePersistentDomain(forName: appDomain)
                portsInit()
                tableView.reloadSections([0,1], with: .automatic)
            }
            if indexPath.row == 1 {
                deleteCoreDataObjectsAlert(tableView.cellForRow(at: indexPath)!)
            }
            if indexPath.row == 2 {
                deleteCoreDataObjectsAlert(tableView.cellForRow(at: indexPath)!)
            }
        default: break
        }
    }
    
    // MARK: - Alerts
    func deleteCoreDataObjectsAlert(_ sender: UITableViewCell) {
        switch sender.reuseIdentifier {
        case "RemoveNonFavorite":
            let alert = UIAlertController(title: "Remove non-favorites network?", message: "This is remove non-favorites network.", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
                CoreDataManager.instance.removeNonFavoriteNetworks()
                NSLog("Non-favorites network removed")
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
                NSLog("Canseled")
            }))
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = sender.frame
            }
            self.present(alert, animated: true, completion: nil)
        case "RemoveAllCoreData":
            let alert = UIAlertController(title: "Remove all application data?", message: "This is remove all data and restore default settings.", preferredStyle: .actionSheet)
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
                CoreDataManager.instance.removeContext()
                NSLog("All saved data removed")
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { _ in
                NSLog("Canseled")
            }))
            if let popoverController = alert.popoverPresentationController {
                popoverController.sourceView = self.view
                popoverController.sourceRect = sender.frame
            }
            self.present(alert, animated: true, completion: nil)
        default:
            NSLog("Cell not selected. But alert is up.")
        }
    }
  
}
