//
//  TabBarController.swift
//  Swift network scanner
//
//  Created by Sergey Evdokimov on 20.05.2018.
//  Copyright © 2018 Sergey Evdokimov. All rights reserved.
//

import UIKit
import FontAwesome_swift

class TabBarController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let tabBarMenu = self.tabBar
        
        let tabScanner = tabBarMenu.items![0]
        tabScanner.title = "Scanner"
        tabScanner.image = UIImage.fontAwesomeIcon(name: .search, textColor: UIColor.blue, size: CGSize(width: 40, height: 40)) // deselect image
        tabScanner.selectedImage = UIImage.fontAwesomeIcon(name: .search, textColor: UIColor.blue, size: CGSize(width: 40, height: 40)) // select image
        
        let tabHistory = tabBarMenu.items![1]
        tabHistory.title = "History"
        tabHistory.image = UIImage.fontAwesomeIcon(name: .history, textColor: UIColor.blue, size: CGSize(width: 40, height: 40))
        tabHistory.selectedImage = UIImage.fontAwesomeIcon(name: .history, textColor: UIColor.blue, size: CGSize(width: 40, height: 40))
        
        let tabSettings = tabBarMenu.items![2]
        tabSettings.title = "Settings"
        tabSettings.image = UIImage.fontAwesomeIcon(name: .sliders, textColor: UIColor.blue, size: CGSize(width: 40, height: 40))
        tabSettings.selectedImage = UIImage.fontAwesomeIcon(name: .sliders, textColor: UIColor.blue, size: CGSize(width: 40, height: 40))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
